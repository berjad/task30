import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent {

  @Input() messageRegister: string;

  @Output() registerAttempt: EventEmitter<any> = new EventEmitter;

  onRegisterClick() {
    this.registerAttempt.emit('Register you must!')
  }

}
