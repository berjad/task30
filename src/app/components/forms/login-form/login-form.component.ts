import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {

  @Input() messageLogin: string;

  @Output() loginAttempt: EventEmitter<any> = new EventEmitter();

  onLoginClick() {
    this.loginAttempt.emit('So you tried to login!?');
  }

}

