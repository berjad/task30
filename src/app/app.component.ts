import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Task30';

  onLoginAttempt(messageLogin) {
    alert("Child says: " + messageLogin);
  }

  onRegisterAttempt(messageRegister) {
    alert("Child wants to inform you: " + messageRegister);
  }
}
